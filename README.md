Figure2Dataset.mat is a Matlab Data file containing bead normalised mean fluorescence data and cell lengths for data used in Figure 2 of Patange et al. 2018.

Data is given for PbolA-GFP WT (WT) and PbolA-GFP delrpoS (DEL) cells.
